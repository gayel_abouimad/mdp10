package com.example.smartheatingfan;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.DisconnectedBufferOptions;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import android.content.Context;
import android.util.Log;

import com.jjoe64.graphview.series.DataPoint;

public class MqttHelper {

    public MqttAndroidClient mqttAndroidClient;
    final String serverUri = "tcp://212.98.137.194:1883";
    final String clientId = "MDP10-CCE";
    final String subscriptionTopic = "nicolas";
    final String username = "iotleb";
    final String password = "iotleb";
    static int i =0;

    public MqttHelper(Context context){

        mqttAndroidClient = new MqttAndroidClient(context, serverUri, clientId);
        mqttAndroidClient.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean b, String s) { Log.w("mqtt", s); }

            @Override
            public void connectionLost(Throwable throwable) {
                Log.w("mqtt", "connection lost");
            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
                Log.w("Mqtt", mqttMessage.toString());
                if(Integer.parseInt(mqttMessage.toString()) > 30){
                    System.out.println("HIGHT----------------------------------");
                    WelcomePage.showsnack();
                }

                String message = mqttMessage.toString();
                if(!message.startsWith("{") ) {
                    WelcomePage.temperature.setText(mqttMessage.toString());
                }
                String TemperatureString = mqttMessage.toString();
                float TemperatureFloat = Float.parseFloat(TemperatureString);
                GraphPage.series.appendData(new DataPoint(i,TemperatureFloat), true, 100);
                GraphPage.updateSeries();
                MqttHelper.i += 5;

            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
                Log.w("Mqtt", "msg delivered");
            }
        });
        connect();
    }

    private void connect(){
        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setAutomaticReconnect(true);
        mqttConnectOptions.setCleanSession(true);
        mqttConnectOptions.setUserName(username);
        mqttConnectOptions.setPassword(password.toCharArray());
        try {
            mqttAndroidClient.connect(mqttConnectOptions, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    DisconnectedBufferOptions disconnectedBufferOptions = new DisconnectedBufferOptions();
                    disconnectedBufferOptions.setBufferEnabled(true);
                    disconnectedBufferOptions.setBufferSize(100);
                    disconnectedBufferOptions.setPersistBuffer(false);
                    disconnectedBufferOptions.setDeleteOldestMessages(false);
                    mqttAndroidClient.setBufferOpts(disconnectedBufferOptions);
                    subscribeToTopic();
                }
                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Log.w("Mqtt", "Failed to connect to: " + serverUri + exception.toString());
                }
            });
        } catch (MqttException ex){
            ex.printStackTrace();
        }
    }

    private void subscribeToTopic() {
        try {
            mqttAndroidClient.subscribe(subscriptionTopic, 0, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    Log.w("Mqtt","Subscribed!");
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Log.w("Mqtt", "Subscribed fail!");
                }
            });

        } catch (MqttException ex) {
            System.err.println("Exception whilst subscribing");
            ex.printStackTrace();
        }
    }

    public void datapulisher(String dataPublish){
        byte[] dataByte = new byte[0];
        try {
//            System.out.println("data:" + dataPublish);
            dataByte = dataPublish.getBytes();
            mqttAndroidClient.publish(subscriptionTopic,dataByte,2,false);
            System.out.println("MSG PUBLISHED SUCCESFULLY");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}