package com.example.smartheatingfan;

import android.graphics.Color;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.content.Intent;
import android.widget.ImageButton;
import org.json.JSONException;
import android.widget.Button;
import android.widget.TextView;

public class WelcomePage extends AppCompatActivity {
    public static MqttHelper mqttHelper;
    private ImageButton OnButton;
    private Button setSpeed;
    private Button setRotation;
    private Button Graph_button;
    private static ImageButton Resistor;
    private boolean IsOn = false;
    private static boolean heatoron = false;
    public static TextView temperature;
    public static ConstraintLayout cl;
    public static Statesaver s1=  new Statesaver();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcomepage);
        cl = findViewById(R.id.buttonContainer);
        startMqtt();
        Graph_button= findViewById(R.id.Graph_button);
        OnButton = findViewById(R.id.onOffBtn2);
        setSpeed = findViewById(R.id.setSpeed);
        setRotation = findViewById(R.id.setRotation);
        temperature = findViewById(R.id.temperature);
        Resistor = findViewById(R.id.resistor);

        OnButton.setOnClickListener(v -> {
            try{
                if(IsOn){
                    mqttHelper.datapulisher(MyJsonClass.JSONint("FanisOn", 0).toString());
                    OnButton.setBackgroundResource(R.drawable.turnon);
                    IsOn = false;
                    setSpeed.setVisibility(View.INVISIBLE);
                    setRotation.setVisibility(View.INVISIBLE);
                    Resistor.setVisibility(View.INVISIBLE);
                    WelcomePage.s1.SetVertRotation(7);
                    WelcomePage.s1.SetHorRotation(7);
                    WelcomePage.s1.SetSpeed(0);

                }else{
                    mqttHelper.datapulisher(MyJsonClass.JSONint("FanisOn", 1).toString());
                    OnButton.setBackgroundResource(R.drawable.turnonblue);
                    IsOn = true;
                    setSpeed.setVisibility(View.VISIBLE);
                    setRotation.setVisibility(View.VISIBLE);
                    Resistor.setVisibility(View.VISIBLE);
                }
            } catch (JSONException e) {
                e.printStackTrace();}
        });

        Resistor.setOnClickListener(v -> {
            try{
                if(heatoron){
                    mqttHelper.datapulisher(MyJsonClass.JSONint("ResistorisOn", 0).toString());
                    Resistor.setBackgroundResource(R.drawable.heator110);
                    heatoron = false;

                }else{
                    mqttHelper.datapulisher(MyJsonClass.JSONint("ResistorisOn", 1).toString());
                    Resistor.setBackgroundResource(R.drawable.heator120);
                    heatoron = true;

                }
            } catch (JSONException e) {
                e.printStackTrace();}
        });

//       LAMBDA FUNCTION
        setSpeed.setOnClickListener(v -> {
            startActivity(new Intent (v.getContext(), SpeedPage.class));
        });

        setRotation.setOnClickListener(v->{
            startActivity(new Intent (v.getContext(), RotationPage.class));
        });

        Graph_button.setOnClickListener(v->{
            startActivity(new Intent (v.getContext(), GraphPage.class));
        });
    }

    private void startMqtt(){
        mqttHelper = new MqttHelper(getApplicationContext());
        System.out.println("MQTT STARTED");
    }

    public static void showsnack(){
        Snackbar sb = Snackbar.make(cl, "Temperature is too High", Snackbar.LENGTH_INDEFINITE).setAction("Fix It", v -> {
            Snackbar sb1 = Snackbar.make(cl, "Resistor Turned Off", Snackbar.LENGTH_SHORT);

            try {
                mqttHelper.datapulisher(MyJsonClass.JSONint("ResistorisOn", 0).toString());
                Resistor.setBackgroundResource(R.drawable.heator110);
                heatoron = false;
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout)sb1.getView();
            layout.setBackgroundColor(Color.BLACK);
            sb1.show();
        });
        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout)sb.getView();
        layout.setBackgroundColor(Color.BLACK);
        sb.show();
    }
}



