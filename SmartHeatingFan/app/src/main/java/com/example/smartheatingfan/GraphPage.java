package com.example.smartheatingfan;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

public class GraphPage extends AppCompatActivity {
    public static LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>();
    public static GraphView graph;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.graphpage);
        graph = findViewById(R.id.graph);
        graph.getGridLabelRenderer().setGridColor(Color.WHITE);
        graph.getGridLabelRenderer().setVerticalLabelsColor(Color.WHITE);
        graph.getGridLabelRenderer().setHorizontalLabelsColor(Color.WHITE);
        graph.getGridLabelRenderer().setLabelVerticalWidth(40);
        graph.getViewport().setScalable(true);
        graph.getViewport().setScalableY(true);
    }

    public static void updateSeries(){
        graph.addSeries(series);
    }
}