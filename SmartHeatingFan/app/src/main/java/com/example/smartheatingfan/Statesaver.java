package com.example.smartheatingfan;

public class Statesaver {
    private double speed=0;
    private double VerticalRotation=7;
    private double HorizontalRotation=7;

    public void SetSpeed(double speed1){
        speed=speed1;
    }
    public void SetVertRotation(double VerticalRotation1){
        VerticalRotation= VerticalRotation1;
    }
    public void SetHorRotation(double HorizontalRotation1){ HorizontalRotation=HorizontalRotation1; }

    public double GetSpeed(){
        return speed;
    }
    public double GetVertRotation(){
        return VerticalRotation;
    }
    public double GetHorRotation(){
        return HorizontalRotation;
    }

}
