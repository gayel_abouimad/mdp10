package com.example.smartheatingfan;
import org.json.JSONException;
import org.json.JSONObject;


public class MyJsonClass {
    public static JSONObject JSONint (String fieldName, int fieldValue) throws JSONException {
        return new JSONObject().put(fieldName,fieldValue);
    }

    public static JSONObject JSONdouble (String fieldName, double fieldValue) throws JSONException {
        return new JSONObject().put(fieldName,fieldValue);
    }
}



