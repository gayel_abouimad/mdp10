package com.example.smartheatingfan;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.SeekBar;
import android.widget.TextView;
import org.json.JSONException;
import org.json.JSONObject;
import android.widget.Button;

import java.util.function.Function;

import static android.widget.SeekBar.*;

public class RotationPage extends AppCompatActivity {
    class ChangeListener implements OnSeekBarChangeListener {

        public ChangeListener(Function<Float, JSONObject> cmd) {
            this.cmd = cmd;
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            cmd.apply((float)seekBar.getProgress());
            JSONObject obj1 = cmd.apply((float)seekBar.getProgress());
            WelcomePage.mqttHelper.datapulisher(obj1.toString());
        }
        private Function<Float, JSONObject> cmd;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rotationpage);

        Button turnoff;
        SeekBar verticalBar;
        SeekBar horizontalBar;
        verticalBar = findViewById(R.id.horizontalSeekBar);
        horizontalBar = findViewById(R.id.VerticalseekBar);
        turnoff = findViewById(R.id.TurnOff);
        verticalBar.setProgress((int) WelcomePage.s1.GetVertRotation());
        horizontalBar.setProgress((int) WelcomePage.s1.GetHorRotation());

        verticalBar.setOnSeekBarChangeListener(
                new RotationPage.ChangeListener(
                        progress -> {
                            WelcomePage.s1.SetVertRotation(verticalBar.getProgress());
                            try {
                                return MyJsonClass.JSONdouble("verticalRotation", progress/100);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            return null;
                        }
                )
        );

        horizontalBar.setOnSeekBarChangeListener(
                new RotationPage.ChangeListener(
                        progress -> {
                            WelcomePage.s1.SetHorRotation(horizontalBar.getProgress());
                            try {
                                return MyJsonClass.JSONdouble("horizontalRotation", progress/100);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            return null;
                        }
                )
        );

        turnoff.setOnClickListener(v -> {
            WelcomePage.s1.SetVertRotation(7);
            WelcomePage.s1.SetHorRotation(7);
            WelcomePage.s1.SetSpeed(0);
            try{
                WelcomePage.mqttHelper.datapulisher(MyJsonClass.JSONint("FanisOn", 0).toString());
            } catch (JSONException e) {
                e.printStackTrace();}
            startActivity(new Intent(v.getContext(), WelcomePage.class));
        });
    }
}