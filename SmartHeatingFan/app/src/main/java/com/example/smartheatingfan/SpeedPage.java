package com.example.smartheatingfan;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.function.Function;

public class SpeedPage extends AppCompatActivity {
    class ChangeListener implements OnSeekBarChangeListener {

        public ChangeListener(Function<Integer, JSONObject> cmd) {
            this.cmd = cmd;
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            cmd.apply(progress);
            WelcomePage.mqttHelper.datapulisher(cmd.apply(progress).toString());
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
        private Function<Integer, JSONObject> cmd;
    }

    public static TextView speedtextView;
    public static SeekBar speedBar;
    private Button gotorot;
    private Button turnoff;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.speedpage);

        speedtextView = findViewById(R.id.speedValue);
        speedBar = findViewById(R.id.speedSeekBar);
        gotorot = findViewById(R.id.goToRotation);
        turnoff = findViewById(R.id.TurnOff);

        speedBar.setProgress((int) WelcomePage.s1.GetSpeed());
        int number=(int)WelcomePage.s1.GetSpeed();
        speedtextView.setText(Integer.toString(number));

        speedBar.setOnSeekBarChangeListener(
            new SpeedPage.ChangeListener(
                progress -> {
                    WelcomePage.s1.SetSpeed(speedBar.getProgress());
                    speedtextView.setText("" + progress);
                    try {
                        if (progress == 0){
                        return MyJsonClass.JSONdouble("speed", progress);
                        }
                        else if(progress == 1){
                            return MyJsonClass.JSONdouble("speed", 0.25);
                        }
                        else if(progress == 2){
                            return MyJsonClass.JSONdouble("speed", 0.5);
                        }
                        else if(progress == 3){
                            return MyJsonClass.JSONdouble("speed", 0.75);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return null;
                }
            )
        );

        gotorot.setOnClickListener(v -> {
            startActivity(new Intent(v.getContext(), RotationPage.class));
        });

        turnoff.setOnClickListener(v -> {
            WelcomePage.s1.SetVertRotation(7);
            WelcomePage.s1.SetHorRotation(7);
            WelcomePage.s1.SetSpeed(0);
            try{
                JSONObject jobj = new MyJsonClass().JSONint("FanisOn", 0);
                WelcomePage.mqttHelper.datapulisher(jobj.toString());
            } catch (JSONException e) {
                e.printStackTrace();}
            Intent myintent = new Intent(v.getContext(), WelcomePage.class);
            startActivity(myintent);
        });
    }
}
